﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; // Needed to write and load files
using System.Threading; // Needed for the multithreading !

public class Tools // Useful class needed for the multithreading
{
	public string Local_Image;
	public byte[] Data;
	public bool completed = false; // Needed later
	
	public void Writing_File() 
    {
        File.WriteAllBytes(Local_Image, Data);
		completed = true;
    }
	
	public void Loading_File() 
    {
        Data = File.ReadAllBytes(Local_Image);
		completed = true;
    }
}

public class Resources_Manager : MonoBehaviour 
{
	Texture2D[] T = new Texture2D[5];
	string[] I = new string[5];
	string[] Local_Names = new string[5];
	
	int number_Threads = 5;
	
	int current_Processes = 0; // Wanted to use a bool here, but we don't know for sure how many processes are running
	
	IEnumerator Image_Loader_FromApp(int id_Image)
    {
		string Local_Image = Application.persistentDataPath + "/" + Local_Names[id_Image];
		
		print("Existing image number "+id_Image+" at : "+Local_Image);
		byte[] Data; // Storage for the data
		T[id_Image] = new Texture2D(0, 0); // Needs to be properly initialized here, to avoid a nasty NullPointer error
		
		if (File.Exists(Local_Image)) // Checking quickly, but is already checked in the main...
		{
			Tools TOOLS = new Tools(); // We need an instanced thread, not a static one, so we need a new class
			TOOLS.Local_Image = Local_Image;
		
			ThreadStart threadDelegate = new ThreadStart(TOOLS.Loading_File);
			Thread newThread = new Thread(threadDelegate);
			newThread.Start();
			while (TOOLS.completed == false) yield return 0; // Important, or it might not work !
		
			T[id_Image].LoadImage(TOOLS.Data); // Loading the data into a texture
		}
		
		current_Processes--;
    }
	
	IEnumerator Image_Loader_FromWeb(int id_Image)
    {
		string Image = I[id_Image];
		string Local_Image = Application.persistentDataPath + "/" + Local_Names[id_Image];
		
		print("Searching image number "+id_Image+" "+Image+" ! To : "+Local_Image);
        WWW Image_Path = new WWW(Image);
		yield return Image_Path; // Waiting for the image to be fully downloaded
		
		// Saving the image on the local data
		
		Tools TOOLS = new Tools(); // We need an instanced thread, not a static one, so we need a new class
		TOOLS.Local_Image = Local_Image;
		TOOLS.Data = Image_Path.bytes;
		
		ThreadStart threadDelegate = new ThreadStart(TOOLS.Writing_File);
        Thread newThread = new Thread(threadDelegate);
        newThread.Start();
		while (TOOLS.completed == false) yield return 0; // Important, or it might not work !
		
		T[id_Image] = Image_Path.texture; // Creating the texture from the image
		current_Processes--;
    }

	// Use this for initialization
	IEnumerator Start ()  // We're using a yield later
	{	
		I[0] = "https://image.ibb.co/ffsfuc/Germanic_Raven.png";  // A bloody and mighty germanic raven !
		I[1] = "https://image.ibb.co/kxd9nx/Picasso.png";  // Some colorful yet dubious painting...
		I[2] = "https://image.ibb.co/mP0FfH/Symbolic_Fox.png";  // A fox... fiery !
		I[3] = "https://image.ibb.co/cOhuaH/Horde_Banner.png"; // Lok'Thar Ogar !
		I[4] = "https://preview.ibb.co/n6NKaH/Cliffside.png"; // The stress-test !
		Local_Names[0] = "GermanicRaven.png";
		Local_Names[1] = "Picasso.png";
		Local_Names[2] = "SymbolicFox.png";
		Local_Names[3] = "HordeBanner.png";
		Local_Names[4] = "Cliffside.png";
		
		for (int i = 0; i < 5; i++)
        {
			if (File.Exists(Application.persistentDataPath + "/" + Local_Names[i])) // Does the image exists on the local data ?
			{
				while (current_Processes >= number_Threads) yield return 0;
				current_Processes++;
				StartCoroutine(Image_Loader_FromApp(i)); // Coroutine, we're using yields inside
			}
			else // If not, need to load it from the Web
			{
				while (current_Processes >= number_Threads) yield return 0;
				current_Processes++;
				StartCoroutine(Image_Loader_FromWeb(i)); // Coroutine, we're using yields inside
			}
		}
		
		while (current_Processes > 0) yield return 0; //Needed to properly fill the texture array !
		Renderer Object_To_Dress = GetComponent<Renderer>();
		System.Random ran = new System.Random();
		
		int Rand_Value = ran.Next(5);
        Object_To_Dress.material.mainTexture = T[Rand_Value];
	}

	// Update is called once per frame
	void Update () 
	{
		// Well... I guess I won't be using this...
		
	}
}
